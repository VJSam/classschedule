package com.vjs.classesschedule;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vjs.classesschedule.extensions.Extensions;

import static com.vjs.classesschedule.extensions.Extensions.CurActivity;

public class SplashActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGHT = 1000;
    private TextView tvIPs;
    private TextView textView2;
    private ImageView splashScreen;

    public SplashActivity() {
        CurActivity = this;
    }
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.setTheme(R.style.AppTheme);
        super.onCreate(icicle);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.splash);

        TextView textSplashVersion = (TextView) findViewById(R.id.textSplashVersion);

        String s = getResources().getString(R.string.version);
        textSplashVersion.setText(textSplashVersion.getText() + s);

        RelativeLayout splashLayout = (RelativeLayout)findViewById(R.id.splashLayout);

//        if (this.getIntent().getIntExtra("NotUseTimeout", 0) == 0) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            setTimeout();
        }
        else {
//            splashLayout.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    finish();
//                    return false;
//                }
//            });

            splashScreen = (ImageView)findViewById(R.id.splashscreen);

            splashScreen.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    textView2.setVisibility(View.VISIBLE);
                    tvIPs.setVisibility(View.VISIBLE);
                    return true;
                }
            });

            if (!Extensions.checkWriteExternalPermission()) {
                String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permissions, 1);
            } else {
                setTimeout();
            }
        }

    }

    @Override
    public void onBackPressed() {
        Destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Destroy();
                } else {
                    Intent myIntent = new Intent(CurActivity, MainActivity.class);
                    CurActivity.startActivity(myIntent);

                    finish();
                }
                break;
            }
            default: super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void Destroy(){
        super.onDestroy();
        finish();
        System.exit(0);
    }

    private void setTimeout() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent myIntent = new Intent(CurActivity, MainActivity.class);
                CurActivity.startActivity(myIntent);

                finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
    }
}