package com.vjs.classesschedule.lessons;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vjs.classesschedule.DicActivity;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.extensions.Extensions;
import com.vjs.classesschedule.modules.BaseListAdapter;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;
import static com.vjs.classesschedule.extensions.Extensions.*;

public class LessonsListAdaper extends BaseListAdapter {

    public LessonsListAdaper(Context context, LessonsClass lessons, View view) {
        super(context, view);
        list = lessons;
    }

    @Override
    public LessonsClass getList() {
        return (LessonsClass)list;
    }
}