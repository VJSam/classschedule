package com.vjs.classesschedule.lessons;

import com.vjs.classesschedule.extensions.BaseClass;

import java.sql.Time;

public class LessonsClass extends BaseClass {

    public LessonsClass() {
    }

    public void add(Integer pID, Time pBeginTime, String pName, String pGroupName, String pPlace) {
        LessonItemClass lessonItem = new LessonItemClass(pID, pBeginTime, pName, pGroupName, pPlace);
        add(lessonItem);
    }

    public int count() {
        return this.size();
    }

    public LessonItemClass getLessonByID(int ID) {
        LessonItemClass res = new LessonItemClass();
        for (int i = 0; i < this.count(); i++) {
            if (this.get(i).ID == ID) {
                res = get(i);
                break;
            }
        }
        return res;
    }

    public LessonItemClass get(int index) {
        return (LessonItemClass) super.get(index);
    }
}
