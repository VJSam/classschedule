package com.vjs.classesschedule.lessons;

import com.vjs.classesschedule.extensions.BaseItemClass;

import java.sql.Time;

/**
 * Created by Victor on 17.10.2017.
 */

public class LessonItemClass extends BaseItemClass {
    public Time BeginTime;
    public String GroupName;
    public String Place;
    public LessonIDsClass LessonIDs = new LessonIDsClass();

    public LessonItemClass(Integer pID, Time pBeginTime, String pName, String pGroupName, String pPlace) {
        super(pID, pName);

        BeginTime = pBeginTime;
        GroupName = pGroupName;
        Place = pPlace;
    }

    public LessonItemClass() {
        super(0, null);
    }

    public String BeginTimeStr() {
        return this.BeginTime.toString().substring(0, 5);
    }

    public class LessonIDsClass {
        public int GroupID;
        public int PairID;
        public int RoomID;
        public int DayID;
    }
}
