package com.vjs.classesschedule;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.vjs.classesschedule.extensions.ExtInterface;
import com.vjs.classesschedule.extensions.Extensions;
import com.vjs.classesschedule.modules.BaseListAdapter;
import com.vjs.classesschedule.modules.PlaceholderFragment;
import com.vjs.classesschedule.modules.SectionsPagerAdapter;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;
import static com.vjs.classesschedule.extensions.Extensions.*;

public class DicActivity extends AppCompatActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    public ViewPager mViewPager;

    public DicActivity() {
        Extensions.CurActivity = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dic);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.dic_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                hideEditFabs();
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                hideEditFabs();
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                hideEditFabs();
                mViewPager.setCurrentItem(tab.getPosition());
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Extensions.showInputDlg(Extensions.CurActivity, getString(R.string.title_add), null,
                        new ExtInterface.OnInputText() {
                    @Override
                    public boolean OnInputText(String text) {
                        switch (mViewPager.getCurrentItem() + 1) {
                            case PAGE_PAIRS: {
                                dbHelper.addLesson(text);
                                break;
                            }
                            case PAGE_ROOMS: {
                                dbHelper.addRoom(text);
                                break;
                            }
                            case PAGE_GROUPS: {
                                dbHelper.addGroup(text);
                                break;
                            }
                        }
                        refreshPage();
                        return false;
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_dic, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshPage() {
        mSectionsPagerAdapter.refreshPage(mViewPager.getCurrentItem());
    }

    void hideEditFabs() {
        ListView list = ((PlaceholderFragment)mSectionsPagerAdapter.getItem(0)).listView;
        if (list != null)
            ((BaseListAdapter)list.getAdapter()).beforeShowFabs(-1);
        list = ((PlaceholderFragment)mSectionsPagerAdapter.getItem(1)).listView;
        if (list != null)
            ((BaseListAdapter)list.getAdapter()).beforeShowFabs(-1);
        list = ((PlaceholderFragment)mSectionsPagerAdapter.getItem(2)).listView;
        if (list != null)
            ((BaseListAdapter)list.getAdapter()).beforeShowFabs(-1);
    }
}
