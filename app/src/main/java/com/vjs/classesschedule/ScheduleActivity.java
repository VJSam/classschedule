package com.vjs.classesschedule;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.*;
import com.vjs.classesschedule.extensions.Extensions;
import com.vjs.classesschedule.extensions.SimpleListAdapter;
import com.vjs.classesschedule.extensions.SimpleListItem;
import com.vjs.classesschedule.lessons.LessonItemClass;
import com.vjs.classesschedule.modules.SpinnerEx;

import java.util.ArrayList;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;
import static com.vjs.classesschedule.extensions.Extensions.ShowAlert;

public class ScheduleActivity extends AppCompatActivity {

    Activity context;
    SpinnerEx cLesson;
    SpinnerEx cRoom;
    SpinnerEx cGroup;
    Button bCancel;
    Button bSave;
    TimePicker timePicker;
    SpinnerEx weekDay;
    int denom = 0;
    private boolean isCancel = true;
    LinearLayout deleteLayout;
    Button bDelete;
    int lessonID = 0;

    public ScheduleActivity() {
        Extensions.CurActivity = this;
        context = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        denom = getIntent().getIntExtra("PageID", denom);

        weekDay = (SpinnerEx)findViewById(R.id.weekDay);
        SimpleListAdapter sa = new SimpleListAdapter(this, Extensions.getWeekDays());
        weekDay.setAdapter(sa);

        timePicker = (TimePicker) this.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        cLesson = (SpinnerEx) findViewById(R.id.cLesson);
        ArrayList<SimpleListItem> lessons = dbHelper.getLessonList();
        SimpleListAdapter adapter = new SimpleListAdapter(this, lessons);
        cLesson.setAdapter(adapter);

        cRoom = (SpinnerEx)findViewById(R.id.cRoom);
        ArrayList<SimpleListItem> rooms = dbHelper.getRoomsList();
        adapter = new SimpleListAdapter(this, rooms);
        cRoom.setAdapter(adapter);

        cGroup = (SpinnerEx)findViewById(R.id.cGroup);
        ArrayList<SimpleListItem> groups = dbHelper.getGroupList();
        adapter = new SimpleListAdapter(this, groups);
        cGroup.setAdapter(adapter);

        bCancel = (Button)findViewById(R.id.bCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.finish();
            }
        });

        lessonID = getIntent().getIntExtra("LessonID", 0);
        if (lessonID > 0) {
            prepareLesson(dbHelper.getLessonByID(lessonID));
            deleteLayout = (LinearLayout)findViewById(R.id.deleteLayout);
            deleteLayout.setVisibility(View.VISIBLE);
            bDelete = (Button)findViewById(R.id.bDelete);
            bDelete.setOnClickListener(deleteClick);
        }

        bSave = (Button)findViewById(R.id.bSave);
        bSave.setOnClickListener(saveClick);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isCancel)
            ((MainActivity)Extensions.Main).refreshPage();
    }

    private void prepareLesson(LessonItemClass lesson) {
        if (lesson.ID <= 0)
            return;

        weekDay.setSelection(weekDay.indexOf(lesson.LessonIDs.DayID));
//        weekDay.setSelection(lesson.LessonIDs.DayID);
        int h = Integer.valueOf(lesson.BeginTime.toString().substring(0, 2));
        int m = Integer.valueOf(lesson.BeginTime.toString().substring(3, 5));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(h);
        } else {
            timePicker.setCurrentHour(h);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setMinute(m);
        } else {
            timePicker.setCurrentMinute(m);
        }


        cLesson.setSelection(cLesson.indexOf(lesson.LessonIDs.PairID));
        cGroup.setSelection(cGroup.indexOf(lesson.LessonIDs.GroupID));
        cRoom.setSelection(cRoom.indexOf(lesson.LessonIDs.RoomID));
    }

    View.OnClickListener saveClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int lID = -1;
            int rlID = -1;
            int gID = -1;

            if (cLesson.getCount() > 0)
                lID = ((TextView)cLesson.getChildAt(0)).getId();
            if (cRoom.getCount() > 0)
                rlID = ((TextView)cRoom.getChildAt(0)).getId();
            if (cGroup.getCount() > 0)
                gID = ((TextView)cGroup.getChildAt(0)).getId();

            if ((lID > 0) & (rlID > 0) & (gID > 0)) {
                int time = 0;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    time = timePicker.getHour() * 100 + timePicker.getMinute();
                } else {
                    time = timePicker.getCurrentHour() * 100 + timePicker.getCurrentMinute();
                }
                int dayID = ((TextView) weekDay.getChildAt(0)).getId();
                if (lessonID > 0) {
                    dbHelper.editSchedule(lessonID, dayID, lID, rlID, gID, time);
                } else
                    dbHelper.addSchedule(lID, rlID, gID, dayID, time, denom == 1);
                isCancel = false;
                context.finish();
            } else {
                ShowAlert(context, getString(R.string.msg_is_empty), true);
            }
        }
    };

    View.OnClickListener deleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Extensions.ShowYesNoDlg(getString(R.string.msg_confirm), getString(R.string.msg_delete_schedule),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dbHelper.removeSchedule(lessonID);
                            isCancel = false;
                            context.finish();
                        }
                    });
        }
    };
}
