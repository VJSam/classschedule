package com.vjs.classesschedule;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import com.vjs.classesschedule.extensions.Extensions;
import com.vjs.classesschedule.modules.MainPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String PREFS_NAME = "com.vjs.classesschedule.ScheduleWidget";
    private static final String PREF_PREFIX_KEY = "cswidget_";

    private Context instance;

    private MainPagerAdapter mSectionsPagerAdapter;

    public ViewPager mViewPager;

    FloatingActionButton fab;
    FloatingActionButton fab_settings;
    FloatingActionButton fab_dic;
    FloatingActionButton fab_add;
    FloatingActionButton fab_refresh;
    TabLayout tabLayout;

    public MainActivity() {
        instance = this;
        Extensions.Main = instance;
        Extensions.CurActivity = instance;
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, final Throwable ex) {
                Log.e("uncaughtException", ex.getMessage(), ex);
                StackTraceElement[] arr = ex.getStackTrace();
                final StringBuffer report = new StringBuffer(ex.toString());
                final String lineSeperator = "-------------------------------\n\n";
                report.append("--------- Stack trace ---------\n\n");
                for (int i = 0; i < arr.length; i++) {
                    report.append( "    ");
                    report.append(arr[i].toString());
                }
                report.append(lineSeperator);
                // If the exception was thrown in a background thread inside
                // AsyncTask, then the actual exception can be found with getCause
                report.append("--------- Cause ---------\n\n");
                Throwable cause = ex.getCause();
                if (cause != null) {
                    report.append(cause.toString());
                    arr = cause.getStackTrace();
                    for (int i = 0; i < arr.length; i++) {
                        report.append("    ");
                        report.append(arr[i].toString());
                    }
                }
                // Getting the Device brand,model and sdk verion details.
                report.append(lineSeperator);
                report.append("--------- Device ---------\n\n");
                report.append("Brand: ");
                report.append(Build.BRAND);
                report.append("Device: ");
                report.append(Build.DEVICE);
                report.append("Model: ");
                report.append(Build.MODEL);
                report.append("Id: ");
                report.append(Build.ID);
                report.append("Product: ");
                report.append(Build.PRODUCT);
                report.append(lineSeperator);
                report.append("--------- Firmware ---------\n\n");
                report.append("Release: ");
                report.append(Build.VERSION.RELEASE);
                report.append("Incremental: ");
                report.append(Build.VERSION.INCREMENTAL);
                report.append(lineSeperator);
                System.exit(0);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Extensions.createDBHelper(this);

        mSectionsPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(fabOnClick);

        fab_settings = (FloatingActionButton) findViewById(R.id.fab_settings);
        fab_dic = (FloatingActionButton) findViewById(R.id.fab_dic);
        fab_add = (FloatingActionButton) findViewById(R.id.fab_add);

        fab_settings.setOnClickListener(setsOnClick);

        fab_dic.setOnClickListener(dicOnClick);

        fab_add.setOnClickListener(addOnClick);

        fab_refresh = (FloatingActionButton) findViewById(R.id.fab_refresh);
        fab_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshPage();
            }
        });

        Extensions.fillDayList();
    }

    void showFab1(FloatingActionButton pFab, double pRight, double pBottom, int pAnimId) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) pFab.getLayoutParams();
        if (fab.getTag() == null) {
            Animation show_fab = AnimationUtils.loadAnimation(getApplication(), pAnimId);
            layoutParams.rightMargin += (int) (pFab.getWidth() * pRight);
            layoutParams.bottomMargin += (int) (pFab.getHeight() * pBottom);
            pFab.setLayoutParams(layoutParams);
            pFab.startAnimation(show_fab);
            pFab.setClickable(true);
        } else {
            Animation hide_fab = AnimationUtils.loadAnimation(getApplication(), pAnimId);
            layoutParams.rightMargin -= (int) (pFab.getWidth() * pRight);
            layoutParams.bottomMargin -= (int) (pFab.getHeight() * pBottom);
            pFab.setLayoutParams(layoutParams);
            pFab.startAnimation(hide_fab);
            pFab.setClickable(false);
        }
    }

    View.OnClickListener fabOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (fab.getTag() == null) {
                showFab1(fab_settings, 1.2, 0, R.anim.fab_sets_show);
                showFab1(fab_dic, 2.4, 0, R.anim.fab_dic_show);
                showFab1(fab_add, 0, 1.2, R.anim.fab_add_show);
                showFab1(fab_refresh, 0, 2.4, R.anim.fab_refresh_show);
                fab.setTag(1);
            } else {
                showFab1(fab_settings, 1.2, 0, R.anim.fab_sets_hide);
                showFab1(fab_dic, 2.4, 0, R.anim.fab_dic_hide);
                showFab1(fab_add, 0, 1.2, R.anim.fab_add_hide);
                showFab1(fab_refresh, 0, 2.4, R.anim.fab_refresh_hide);
                fab.setTag(null);
            }
        }
    };

    View.OnClickListener setsOnClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            fab.callOnClick();
            Intent myIntent = new Intent(instance, SettingsActivity.class);
            instance.startActivity(myIntent);
        }
    };

    View.OnClickListener dicOnClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            fab.callOnClick();
            Extensions.isStartActivity(DicActivity.class);
        }
    };

    View.OnClickListener addOnClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            fab.callOnClick();
            Intent myIntent = new Intent(instance, ScheduleActivity.class);
            myIntent.putExtra("PageID",  mViewPager.getCurrentItem());
            instance.startActivity(myIntent);
        }
    };

    @Override
    public void onDestroy() {
        moveTaskToBack(true);
        super.onDestroy();
        finish();
        System.exit(0);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_report_problem_black_24dp)
                .setTitle(getResources().getString(R.string.msg_confirm))
                .setMessage(getResources().getString(R.string.msg_close_app))
                .setNegativeButton(getResources().getString(R.string.btn_no), null)
                .setPositiveButton(getResources().getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onDestroy();
                    }

                })
                .show();
    }

    public void refreshPage() {
        mSectionsPagerAdapter.refreshPage(mViewPager.getCurrentItem());
    }

    public static CharSequence loadTitlePref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String titleValue = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null);
        if (titleValue != null) {
            return titleValue;
        } else {
            return context.getString(R.string.app_name);
        }

    }
}
