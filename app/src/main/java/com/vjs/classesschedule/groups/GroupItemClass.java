package com.vjs.classesschedule.groups;

import com.vjs.classesschedule.extensions.BaseItemClass;

/**
 * Created by Victor on 17.10.2017.
 */

public class GroupItemClass extends BaseItemClass {

    public GroupItemClass(int pID, String pName) {
        super(pID, pName);
    }
}
