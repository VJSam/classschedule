package com.vjs.classesschedule.groups;

import com.vjs.classesschedule.extensions.BaseClass;

public class GroupsClass extends BaseClass {

    public GroupsClass() {
    }

    public void add(Integer pID, String pName) {
        GroupItemClass lessonItem = new GroupItemClass(pID, pName);
        add(lessonItem);
    }

    public int count() {
        return this.size();
    }

    public GroupItemClass get(int index) {
        return (GroupItemClass) super.get(index);
    }
}
