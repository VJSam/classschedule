package com.vjs.classesschedule.groups;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.modules.BaseListAdapter;

import static com.vjs.classesschedule.extensions.Extensions.PAGE_GROUPS;

public class GroupsListAdaper extends BaseListAdapter {

    public GroupsListAdaper(Context context, GroupsClass groups, View view) {
        super(context, view);
        list = groups;
    }

    @Override
    public GroupsClass getList() {
        return (GroupsClass) list;
    }
    
}