package com.vjs.classesschedule.modules;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.vjs.classesschedule.R;

import java.util.ArrayList;

public class MainPagerAdapter extends FragmentPagerAdapter {
    private Context instance;
    ArrayList<MainFragment> listFragment;

    public MainPagerAdapter(FragmentManager fm, Context MainInstance) {
        super(fm);
        instance = MainInstance;
        listFragment = new ArrayList<MainFragment>();
        listFragment.add(MainFragment.newInstance(0, instance));
        listFragment.add(MainFragment.newInstance(1, instance));
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        return MainFragment.newInstance(position + 1, instance);
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return instance.getString(R.string.nom_caption);
            case 1:
                return instance.getString(R.string.denom_caption);
        }
        return null;
    }

    public void refreshPage(int PageId) {
        listFragment.get(PageId).refresh();
    }
}
