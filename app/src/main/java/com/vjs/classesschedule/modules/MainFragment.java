package com.vjs.classesschedule.modules;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.extensions.*;
import com.vjs.classesschedule.lessons.LessonItemClass;

public class MainFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private ExpandableListView expandableListView;
    public int section_id;

    DataClass header;
    View rootView;
    Context instance;

    public MainFragment() {

    }

    @SuppressLint("ValidFragment")
    public MainFragment(Context MainInst) {
        super();
        instance = MainInst;
        header = new DataClass(instance);
    }

    public static MainFragment newInstance(int sectionNumber, Context MainInstance) {
        MainFragment fragment = new MainFragment(MainInstance);
        fragment.section_id = sectionNumber;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        refresh();
//        expandableListView.setOnChildClickListener(childClickListener);
        return rootView;
    }

    void setItems() {

    }

    ExpandableListView.OnChildClickListener childClickListener = new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            DayItemClass di = header.get(section_id).get(groupPosition);
            LessonItemClass li = di.Lessons.get(childPosition);

            Extensions.makeText(li.BeginTimeStr() + " : " + li.Name);
            return false;
        }
    };

    public void refresh() {
        expandableListView = (ExpandableListView)rootView.findViewById(R.id.simple_expandable_listview);

        expandableListView.setGroupIndicator(null);

        DaysClass dc = header.get(section_id);
        dc.fillLessons();

        ExpandableListAdapter adapter = new ExpandableListAdapter(instance, dc, rootView);

        expandableListView.setAdapter(adapter);
        expandableListView.setScrollBarStyle(0x03000000);
        expandableListView.setVerticalScrollBarEnabled(true);
        expandableListView.setOnChildClickListener(childClickListener);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                ((ExpandableListAdapter)parent.getExpandableListAdapter()).beforeShowFabs(-1);
                return false;
            }
        });
    }
}