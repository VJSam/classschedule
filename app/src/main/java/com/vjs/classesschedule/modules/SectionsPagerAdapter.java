package com.vjs.classesschedule.modules;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.vjs.classesschedule.R;

import java.util.ArrayList;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    Context instance;
    ArrayList<PlaceholderFragment> listFragment;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        listFragment = new ArrayList<PlaceholderFragment>();
        instance = context;
        listFragment.add(PlaceholderFragment.newInstance(instance, 0));
        listFragment.add(PlaceholderFragment.newInstance(instance, 1));
        listFragment.add(PlaceholderFragment.newInstance(instance, 2));
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return listFragment.get(position);
//        return PlaceholderFragment.newInstance(instance, position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return instance.getString(R.string.tab_pairs);
            case 1:
                return instance.getString(R.string.tab_places);
            case 2:
                return instance.getString(R.string.tab_groups);
        }
        return null;
    }

    public void refreshPage(int PageId) {
        listFragment.get(PageId).refresh();
    }
}