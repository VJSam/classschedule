package com.vjs.classesschedule.modules;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.Spinner;
import com.vjs.classesschedule.extensions.SimpleListItem;

public class SpinnerEx extends android.support.v7.widget.AppCompatSpinner {

    public SpinnerEx(Context context) {
        super(context);
    }

    public SpinnerEx(Context context, int mode) {
        super(context, mode);
    }

    public SpinnerEx(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerEx(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SpinnerEx(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public SpinnerEx(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }

    public int indexOf(String value) {
        int res = -1;

        for (int i = 0; i < getCount(); i++) {
            if (((SimpleListItem)getItemAtPosition(i)).Name.equals(value)) {
                res = i;
                break;
            }
        }

        return res;
    }

    public int indexOf(int id) {
        int res = -1;

        for (int i = 0; i < getCount(); i++) {
            if (((SimpleListItem)getItemAtPosition(i)).ID == id) {
                res = i;
                break;
            }
        }

        return res;
    }
}
