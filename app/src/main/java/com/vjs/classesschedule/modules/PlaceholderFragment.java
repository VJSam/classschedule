package com.vjs.classesschedule.modules;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.extensions.SimpleListItem;
import com.vjs.classesschedule.groups.GroupsClass;
import com.vjs.classesschedule.groups.GroupsListAdaper;
import com.vjs.classesschedule.lessons.LessonsClass;
import com.vjs.classesschedule.lessons.LessonsListAdaper;
import com.vjs.classesschedule.rooms.RoomsClass;
import com.vjs.classesschedule.rooms.RoomsListAdaper;

import java.util.ArrayList;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;
import static com.vjs.classesschedule.extensions.Extensions.*;

public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    public int section_id;

    private GroupsClass groups;
    private LessonsClass lessons;
    private RoomsClass rooms;
    public ListView listView;
    public GroupsListAdaper gla;
    public LessonsListAdaper lla;
    public RoomsListAdaper rla;
    private Context instance;
    private View rootView;

    public PlaceholderFragment() {
    }

    @SuppressLint("ValidFragment")
    public PlaceholderFragment(Context context) {
        instance = context;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(Context context, int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment(context);
        fragment.section_id = sectionNumber;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dic, container, false);
        refresh();
        return rootView;
    }

    public void refresh() {
        switch (section_id + 1) {
            case PAGE_PAIRS: {
                ArrayList<SimpleListItem> list = dbHelper.getLessonList();
                if (list != null) {
                    lessons = new LessonsClass();
                    for (SimpleListItem item : list) {
                        lessons.add(item.ID, null, item.Name, null, null);
                    }
                    listView = (ListView) rootView.findViewById(R.id.listView);
                    lla = new LessonsListAdaper(instance, lessons, rootView);
                    listView.setAdapter(lla);
                    listView.setOnTouchListener(listTouch);
                }
                break;
            }
            case PAGE_ROOMS : {
                ArrayList<SimpleListItem> list = dbHelper.getRoomsList();
                if (list != null) {
                    rooms = new RoomsClass();
                    for (SimpleListItem item : list) {
                        rooms.add(item.ID, item.Name);
                    }
                    listView = (ListView) rootView.findViewById(R.id.listView);
                    rla = new RoomsListAdaper(instance, rooms, rootView);
                    listView.setAdapter(rla);
                    listView.setOnTouchListener(listTouch);
                }
                break;
            }
            case PAGE_GROUPS : {
                ArrayList<SimpleListItem> list = dbHelper.getGroupList();
                if (list != null) {
                    groups = new GroupsClass();
                    for (SimpleListItem item : list) {
                        groups.add(item.ID, item.Name);
                    }
                    listView = (ListView) rootView.findViewById(R.id.listView);
                    gla = new GroupsListAdaper(instance, groups, rootView);
                    listView.setAdapter(gla);
                    listView.setOnTouchListener(listTouch);
                }
                break;
            }
        }
    }

    public View.OnTouchListener listTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                ((BaseListAdapter)((ListView)v).getAdapter()).beforeShowFabs(-1);

            return false;
        }
    };
}

