package com.vjs.classesschedule.modules;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vjs.classesschedule.DicActivity;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.extensions.BaseClass;
import com.vjs.classesschedule.extensions.BaseItemClass;
import com.vjs.classesschedule.extensions.ExtInterface;
import com.vjs.classesschedule.extensions.Extensions;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;
import static com.vjs.classesschedule.extensions.Extensions.*;

public abstract class BaseListAdapter extends BaseAdapter {
    public LayoutInflater mInflater;
    public Context instance;
    public View fragView;
    public BaseClass list;

    public static class ViewHolder {
        public TextView textView;
        public FloatingActionButton fab_Edit;
        public FloatingActionButton fab_Delete;
    }

    public BaseListAdapter(Context context, View view) {
        mInflater = LayoutInflater.from(context);
        instance = context;
        fragView = view;
    }

    @Override
    public int getCount() {
        return list.count();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = null;
        if (convertView == null)
            row = CreateLayout(position, convertView);
        else
            row = convertView;
        return row;
    }

    public void beforeShowFabs(int pID) {
        ListView lv = ((ListView)((RelativeLayout) fragView).getChildAt(0));
        for (int i = 0; i < lv.getChildCount(); i++) {
            ViewHolder h = (ViewHolder)lv.getChildAt(i).getTag();
            Extensions.EditMenu m = (Extensions.EditMenu) h.textView.getTag();
            if (pID == -1) {
                if (!m.ToShow)
                    Extensions.showDicMenu(h.textView, new FloatingActionButton[]{h.fab_Edit, h.fab_Delete});
            } else {
                if ((!m.ToShow) & (m.ID != pID)) {
                    Extensions.showDicMenu(h.textView, new FloatingActionButton[]{h.fab_Edit, h.fab_Delete});
                }
            }
        }
    }

    public BaseClass getList() {
        return null;
    }

    public View CreateLayout(int position, View convertView) {
        final ViewHolder holder;

        View rowView = convertView;
        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.dic_item, null, true);
            holder = new ViewHolder();

            holder.fab_Edit = (FloatingActionButton) rowView.findViewById(R.id.fab_Edit);
            holder.fab_Delete = (FloatingActionButton) rowView.findViewById(R.id.fab_Delete);

            holder.textView = (TextView) rowView.findViewById(R.id.textDicItem);
            holder.textView.setText(list.get(position).Name);
            holder.textView.setId(list.get(position).ID);
//            holder.textView.setTag(PAGE_PAIRS);
            Extensions.EditMenu m = new Extensions.EditMenu();
            m.ID = list.get(position).ID;
            m.PageID = PAGE_PAIRS;
            m.sObject = getList().get(position);
            holder.textView.setTag(m);
            holder.fab_Edit.setId(m.ID);
            holder.fab_Delete.setId(m.ID);
            holder.fab_Edit.setClickable(false);
            holder.fab_Delete.setClickable(false);

            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beforeShowFabs(v.getId());
                    Extensions.showDicMenu(v, new FloatingActionButton[] {holder.fab_Edit, holder.fab_Delete});
                }
            });

            holder.fab_Edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = ((BaseItemClass)((EditMenu)holder.textView.getTag()).sObject).Name;
                    
                    Extensions.showInputDlg(CurActivity, CurActivity.getString(R.string.title_edit), name,
                            new ExtInterface.OnInputText() {
                                @Override
                                public boolean OnInputText(String text) {
                                    int vID = holder.textView.getId();
                                    switch (((DicActivity)CurActivity).mViewPager.getCurrentItem() + 1) {
                                        case PAGE_PAIRS: {
                                            if (!text.isEmpty())
                                                dbHelper.editLesson(vID, text);
                                            break;
                                        }
                                        case PAGE_ROOMS: {
                                            if (!text.isEmpty())
                                                dbHelper.editRoom(vID, text);
                                            break;
                                        }
                                        case PAGE_GROUPS: {
                                            if (!text.isEmpty())
                                                dbHelper.editGroup(vID, text);
                                            break;
                                        }
                                    }
                                    ((DicActivity)CurActivity).refreshPage();
                                    return false;
                                }
                            });
                }
            });

            holder.fab_Delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int vID = v.getId();
                    ShowYesNoDlg(CurActivity.getString(R.string.msg_confirm), "Really delete?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (((DicActivity)CurActivity).mViewPager.getCurrentItem() + 1) {
                                case PAGE_PAIRS: {
                                    dbHelper.removeLesson(vID);
                                    break;
                                }
                                case PAGE_ROOMS: {
                                    dbHelper.removeRooms(vID);
                                    break;
                                }
                                case PAGE_GROUPS: {
                                    dbHelper.removeGroup(vID);
                                    break;
                                }
                            }
                            ((DicActivity) CurActivity).refreshPage();

                        }
                    });
                }
            });

            rowView.setTag(holder);
        }
        return rowView;
    }
}
