package com.vjs.classesschedule.extensions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vjs.classesschedule.MainActivity;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.ScheduleActivity;
import com.vjs.classesschedule.lessons.LessonItemClass;

import java.util.ArrayList;

/**
 * Created by farooq on 1/4/2017.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private DaysClass header; // header titles
    private View fragView;
    ViewHolder holder;
    ArrayList<View> listView = new ArrayList<>();
    // Child data in format of header title, child title

    public static class ViewHolder {
        public TextView child_text;
        public TextView child_time;
        public TextView child_place;
        public TextView child_group;

        public FloatingActionButton fab_Edit;
        public FloatingActionButton fab_Delete;

        public Extensions.EditMenu edit_menu;

        public Object getTag() {
            return edit_menu;
        }
    }

    public ExpandableListAdapter(Context context, DaysClass listDataHeader, View view) {
        this._context = context;
        this.header = listDataHeader;
        fragView = view;
    }

    @Override
    public int getGroupCount() {
        // Get header size
        return this.header.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        return this.header.get(groupPosition).Lessons.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        // Get header position
        return this.header.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // This will return the child
        return this.header.get(groupPosition).Lessons.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, parent, false);
        }

        // Getting header title
        DayItemClass di = ((DayItemClass)getGroup(groupPosition));
        String headerTitle = di.getDayName();
        if (di.countLessons() > 0)
            headerTitle = String.format("%1$s - %2$d " + Extensions.Main.getString(R.string.pairs),
                    di.getDayName(), di.countLessons());

        //set content for the parent views
        TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(headerTitle);

        // If group is expanded then change the text into bold and change the
        // icon
        int lc = di.countLessons();
        header_text.setTypeface(null, Typeface.NORMAL);
        header_text.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        if (lc > 0) {
            if (isExpanded) {
                header_text.setTypeface(null, Typeface.BOLD);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_36dp, 0);
//                } else {
//                    header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up, 0);
//                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    header_text.setBackground(this._context.getResources().getDrawable(R.drawable.btn_blue_pink_glossy));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        header_text.setBackground(this._context.getResources().getDrawable(R.drawable.btn_blue_pink_glossy, null));
                    }
                }

            } else {
                // If group is not expanded then change the text back into normal
                // and change the icon

                header_text.setTypeface(null, Typeface.NORMAL);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0,  R.drawable.ic_expand_more_black_36dp, 0);
//                } else {
//                    header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down, 0);
//                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    header_text.setBackground(this._context.getResources().getDrawable(R.drawable.btn_blue_glossy));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        header_text.setBackground(this._context.getResources().getDrawable(R.drawable.btn_blue_glossy, null));
                    }
                }
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting child text
        LessonItemClass li = ((LessonItemClass)getChild(groupPosition, childPosition));
        final String childText =String.format("%1$s %2$s\r\n%4$s %3$s",
                li.BeginTimeStr(), li.Name, li.GroupName, li.Place);
        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, parent, false);
        }

        holder = new ViewHolder();
        Extensions.EditMenu m = new Extensions.EditMenu();
        m.ID = li.ID;
        m.PageID = header.getPageID() + 1;
//        if (header.vIsDenom)
//            m.PageID = 2;
        m.sObject = li;
        holder.edit_menu = m;

        //set content in the child views
        holder.child_text = (TextView) convertView.findViewById(R.id.childLesson);
        holder.child_text.setText(li.Name);
        holder.child_text.setOnClickListener(itemClickListener);

        holder.child_time = (TextView) convertView.findViewById(R.id.childTime);
        holder.child_time.setText(li.BeginTimeStr());
        holder.child_time.setOnClickListener(itemClickListener);

        holder.child_place = (TextView) convertView.findViewById(R.id.childPlace);
        holder.child_place.setText(li.Place);
        holder.child_place.setOnClickListener(itemClickListener);

        holder.child_group = (TextView) convertView.findViewById(R.id.childGroup);
        holder.child_group.setText(li.GroupName);
        holder.child_group.setOnClickListener(itemClickListener);

        holder.fab_Edit = (FloatingActionButton) convertView.findViewById(R.id.fab_Edit);
        holder.fab_Delete = (FloatingActionButton) convertView.findViewById(R.id.fab_Delete);
        convertView.setTag(holder);

//        int ind = listView.indexOf(convertView);
//        if (ind < 0)
//            listView.add(convertView);
//        else
//            listView.set(ind, convertView);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }


    public View.OnClickListener itemClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            LessonItemClass li = (LessonItemClass) ((ViewHolder)((RelativeLayout)v.getParent()).getTag()).edit_menu.sObject;
            Intent myIntent = new Intent(Extensions.Main, ScheduleActivity.class);
            myIntent.putExtra("PageID",  ((MainActivity)Extensions.Main).mViewPager.getCurrentItem());
            myIntent.putExtra("LessonID",  li.ID);
            Extensions.Main.startActivity(myIntent);
//            ViewHolder h = (ViewHolder) ((RelativeLayout)v.getParent()).getTag();
//            beforeShowFabs(h.edit_menu.ID);
//            Extensions.showDicMenu(h, new FloatingActionButton[] {h.fab_Edit, h.fab_Delete}, h.edit_menu.ToShow);
        }
    };


}
