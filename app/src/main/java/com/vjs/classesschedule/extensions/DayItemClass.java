package com.vjs.classesschedule.extensions;

import com.vjs.classesschedule.lessons.LessonsClass;

import java.sql.Time;

/**
 * Created by Victor on 17.10.2017.
 */

public class DayItemClass {
    private Integer dayID = 0;

    public LessonsClass Lessons;

    public DayItemClass() {
        Lessons = new LessonsClass();
    }

    public DayItemClass(Integer pDayID) {
        this();
        setDayID(pDayID);
    }

    public Integer getDayID(){
        return dayID;
    }

    public String getDayName(){
        String res = "";
        try{
            res = Extensions.daysList.get(dayID - 1);
        } catch (Exception ex){}

        return res;
    }

    public void setDayID(Integer value){
        dayID = value;
    }

    public void update(DayItemClass object) {
        dayID = object.getDayID();
        Lessons = object.Lessons;
    }

    public void createTestLessons() {
        Lessons.add(1, Time.valueOf("08:00:00"), "Pair1", "Group1", "201");
        Lessons.add(2, Time.valueOf("10:00:00"), "Pair2", "Group2", "202");
        Lessons.add(3, Time.valueOf("12:10:00"), "Pair3", "Group3", "203");
        Lessons.add(4, Time.valueOf("14:00:00"), "Pair4", "Group4", "204а");
    }

    public int countLessons() {
        return Lessons.size();
    }
}

