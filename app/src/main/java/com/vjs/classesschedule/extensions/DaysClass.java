package com.vjs.classesschedule.extensions;

import java.util.ArrayList;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;

public class DaysClass extends ArrayList<DayItemClass> {

     boolean vIsDenom = false;

    public DaysClass(boolean IsDenom) {
        vIsDenom = IsDenom;
        prepareDays();
    }

    public ArrayList<String> getDaysList() {
        return Extensions.daysList;
    }

    @Override
    public DayItemClass get(int index) {
        if ((index > 0) & (index >= size()))
            index = size() - 1;
        return super.get(index);
    }

    @Override
    public boolean add(DayItemClass object) {
        if (object.getDayID() == 0) {
            object.setDayID(1);
            super.add(object);
        } else {
            if (size() == 0)
                super.add(object);
            else {
                DayItemClass di = this.getByDayId(object.getDayID());
                if (di != null)
                    di.update(object);
                else
                    super.add(object);
            }
        }
        return true;
    }

    public DayItemClass getByDayId(Integer pDayId) {
        DayItemClass res = null;
        for (DayItemClass di : this) {
            if (di.getDayID() == pDayId) {
                res = di;
                break;
            }
        }
        return res;
    }

    public void prepareDays() {
        this.add(new DayItemClass(1));
        this.add(new DayItemClass(2));
        this.add(new DayItemClass(3));
        this.add(new DayItemClass(4));
        this.add(new DayItemClass(5));
        this.add(new DayItemClass(6));
        this.add(new DayItemClass(7));
    }

    public void fillLessons() {
        clear();
        prepareDays();
        for (DayItemClass di : dbHelper.getScheduleList(vIsDenom)) {
            DayItemClass dic = getByDayId(di.getDayID());
            if (dic != null) {
                dic.Lessons = null;
                dic.Lessons = di.Lessons;
            }
        }
        for (int i = 0; i < size(); i++) {
            if (get(i).countLessons() == 0) {
                remove(i);
                i--;
            }

        }
    }

    public int getPageID() {
        return vIsDenom ? 0 : 1;
    }
}

