package com.vjs.classesschedule.extensions;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.vjs.classesschedule.R;

import java.util.ArrayList;

public class SimpleListAdapter extends BaseAdapter {

    ArrayList<SimpleListItem> list;
    private Context context;
    private TextView txtTitle;

    public SimpleListAdapter(Context context, ArrayList<SimpleListItem> simpleList) {
        this.context = context;
        list = simpleList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_spinner_dropdown_item, null);
//            convertView = mInflater.inflate(R.layout.spinner_item, null);
        }

        txtTitle = (TextView) convertView.findViewById(R.id.spinItem);
//        txtTitle = (TextView) convertView.findViewById(R.id.comboItem);

        if (txtTitle != null) {
            txtTitle.setText(list.get(position).Name);
            txtTitle.setId(list.get(position).ID);
        }
        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_spinner_dropdown_item, null);
        }

        if (convertView instanceof TextView)
            txtTitle = (TextView) convertView;

        if (txtTitle != null) {
            txtTitle.setText(list.get(position).Name);
            txtTitle.setId(list.get(position).ID);
        }
        return convertView;
    }

//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        return null;
//    }
//
//    @Override
//    public void registerDataSetObserver(DataSetObserver observer) {
//
//    }
//
//    @Override
//    public void unregisterDataSetObserver(DataSetObserver observer) {
//
//    }
//
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return list.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return list.indexOf(list.get(position));
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        return null;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return 0;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 0;
//    }
//
//    @Override
//    public boolean isEmpty() {
//        return ((list == null) || (list.size() == 0));
//    }
}
