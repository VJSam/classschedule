package com.vjs.classesschedule.extensions;

import android.content.Context;
import android.support.annotation.NonNull;

import com.vjs.classesschedule.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Victor on 17.10.2017.
 */

public class DataClass {

    private Context parent;

    private DaysClass Nom;
    private DaysClass Denom;

    public DataClass(Context instance) {
        parent = instance;
        Nom = new DaysClass(false);
        Denom = new DaysClass(true);

//        Nom.fillLessons();
//        Denom.fillLessons();

//        Nom.get(0).createTestLessons();
//        Nom.get(1).createTestLessons();
//        Denom.get(1).createTestLessons();
//        Denom.get(2).createTestLessons();
    }

    public ArrayList<String> getDaysList() {
        return Nom.getDaysList();
    }

    public DaysClass getNom() {
        return Nom;
    }

    public DaysClass getDenom() {
        return Denom;
    }

    public DaysClass get(Integer index) {
        if (index == 0)
            return getNom();
        else
            return getDenom();
    }
}
