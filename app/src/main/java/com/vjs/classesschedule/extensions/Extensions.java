package com.vjs.classesschedule.extensions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.vjs.classesschedule.R;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

import static com.vjs.classesschedule.extensions.DBHelper.dbHelper;

/**
 * Created by Victor on 17.10.2017.
 */

public class Extensions {
    public static Context Main;
    public static Context CurActivity;
    public static final int PAGE_PAIRS = 1;
    public static final int PAGE_ROOMS = 2;
    public static final int PAGE_GROUPS = 3;

    public static ArrayList<String> daysList;

    public static void fillDayList(){
        daysList = new ArrayList<String>();
        daysList.add(Extensions.Main.getString(R.string.Monday));
        daysList.add(Extensions.Main.getString(R.string.Tuesday));
        daysList.add(Extensions.Main.getString(R.string.Wednesday));
        daysList.add(Extensions.Main.getString(R.string.Thursday));
        daysList.add(Extensions.Main.getString(R.string.Friday));
        daysList.add(Extensions.Main.getString(R.string.Saturday));
        daysList.add(Extensions.Main.getString(R.string.Sunday));
    }

    public static View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ExpandableListView ev = ((ExpandableListView)v.getParent().getParent());
            ev.performItemClick(v, ev.getPositionForView(v), 0);
        }
    };

    public static boolean isStartActivity(Class<?> cls) {
            Intent myIntent = new Intent(Main, cls);
            Main.startActivity(myIntent);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            return true;
    }

    public static void createDBHelper(Context context) {
        dbHelper.getInstance(context);
//        new DBHelper(context);
    }

    public static void showDicMenu(View v, FloatingActionButton[] fabs) {
//        final int vID = v.getId() ;
//        final int vPageId = Integer.valueOf(v.getTag().toString());
        final EditMenu m = (EditMenu)v.getTag();

        for (FloatingActionButton fab : fabs) {
            if (m.ToShow)
                showFab(fab, 0, 0, R.anim.fab_simple_show, m.ToShow);
            else
                showFab(fab, 0, 0, R.anim.fab_simple_hide, m.ToShow);
        }

        m.ToShow = !m.ToShow;
    }

    public static void showDicMenu(ExpandableListAdapter.ViewHolder v, FloatingActionButton[] fabs, Boolean toShow) {
//        final int vID = v.getId() ;
//        final int vPageId = Integer.valueOf(v.getTag().toString());
        final EditMenu m = v.edit_menu;

        for (FloatingActionButton fab : fabs) {
            if (toShow)
                showFab(fab, 0, 0, R.anim.fab_simple_show, m.ToShow);
            else
                showFab(fab, 0, 0, 0, m.ToShow);
        }

        m.ToShow = !toShow;
    }

    public static void ShowYesNoDlg(String title, String text, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CurActivity);
        builder.setTitle(title);
        builder.setMessage(text);

        builder.setPositiveButton(CurActivity.getText(R.string.btn_yes), onClickListener);

        builder.setNegativeButton(CurActivity.getText(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public static void showInputDlg(Context context, String title, @Nullable String text, final ExtInterface.OnInputText mInputText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
        if (text != null)
            input.setText(text);
        input.setFocusable(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            input.setShowSoftInputOnFocus(true);
        }
        builder.setView(input);

        builder.setPositiveButton(context.getText(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt = input.getText().toString();
                if (mInputText != null)
                    mInputText.OnInputText(txt);
            }
        });
        builder.setNegativeButton(context.getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    showKeyboard(input, false);
                }
            });
        }

        builder.show();
        showKeyboard(input, true);
    }

    public enum ButtonType {
        Positive,
        Neutral,
        Negative
    }

    public static class DialogButton {
        public ButtonType Type = ButtonType.Neutral;
        public String Text = "";
        public DialogInterface.OnClickListener Listener = null;
    }

    public static class DialogButtons extends ArrayList<DialogButton> {
        public DialogButtons() {}
    }

    public static AlertDialog ShowDialog(Context context, String title, String[] strings, DialogButtons buttons) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        if (buttons != null) {
            for (int i = 0; i < buttons.size(); i++ ) {
                switch (buttons.get(i).Type) {
                    case Negative: ad.setNegativeButton(buttons.get(i).Text, buttons.get(i).Listener);
                        break;
                    case Positive: ad.setPositiveButton(buttons.get(i).Text, buttons.get(i).Listener);
                        break;
                    case Neutral:  ad.setNeutralButton(buttons.get(i).Text, buttons.get(i).Listener);
                        break;
                }
            }
        }
        ad.setItems(strings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
            }
        });
        return ad.show();
    }

    public static void makeText(String text) {
        Toast.makeText(CurActivity, text, Toast.LENGTH_SHORT).show();
    }

    public static void makeText(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    static void showFab(FloatingActionButton pFab, double pRight, double pBottom, int pAnimId, boolean toShow) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) pFab.getLayoutParams();
        if (toShow) {
            layoutParams.rightMargin += (int) (pFab.getWidth() * pRight);
            layoutParams.bottomMargin += (int) (pFab.getHeight() * pBottom);
            pFab.setLayoutParams(layoutParams);
            if (pAnimId != 0) {
                Animation show_fab = AnimationUtils.loadAnimation(CurActivity, pAnimId);
                pFab.startAnimation(show_fab);
            }
            pFab.setClickable(true);
        } else {
            layoutParams.rightMargin -= (int) (pFab.getWidth() * pRight);
            layoutParams.bottomMargin -= (int) (pFab.getHeight() * pBottom);
            pFab.setLayoutParams(layoutParams);
            if (pAnimId != 0) {
                Animation hide_fab = AnimationUtils.loadAnimation(CurActivity, pAnimId);
                pFab.startAnimation(hide_fab);
            }
            pFab.setClickable(false);
        }
    }

    public static class EditMenu {
        public int ID = 0;
        public int PageID = 0;
        public boolean ToShow = true;
        public Object sObject = null;
    }

    public static void showKeyboard(EditText input, boolean toShow) {
        InputMethodManager imm = (InputMethodManager)CurActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (toShow) {
            input.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
        else {
            input.clearFocus();
            Activity activity = (Activity)CurActivity;
            if ((activity.getCurrentFocus() != null) && (activity.getCurrentFocus().getWindowToken() != null))
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static ArrayList<SimpleListItem> getWeekDays() {
        DateFormatSymbols dfs = new DateFormatSymbols();

        ArrayList<SimpleListItem> res = new ArrayList<>();

        String[] wd = dfs.getWeekdays();
        for (int i = 2; i < wd.length; i++) {
            res.add(new SimpleListItem(i - 1, wd[i].toString().toUpperCase(), 0));
        }
        res.add(new SimpleListItem(7, wd[1].toString().toUpperCase(), 0));

        return res;
    }

    public static boolean checkWriteExternalPermission()
    {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = CurActivity.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static void ShowAlert(Context activity, String message, Boolean isError) {
        TextView title = new TextView(activity);
        // You Can Customise your Title here
        String titleTxt = "";
        int textColor = 0;
        if (isError) {
            titleTxt = activity.getResources().getString(R.string.msg_title_error);
            textColor = Color.RED;
        }
        else {
            titleTxt = activity.getResources().getString(R.string.msg_title_info);
            textColor = Color.WHITE;
        }

        title.setText(titleTxt);
        title.setBackgroundColor(Color.DKGRAY);
//        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(16);


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCustomTitle(title);
        //builder.setIcon(R.drawable.alert_36);

        builder.setMessage(message);

        builder.setCancelable(true);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.show();
        TextView messageText = (TextView)alert.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.LEFT);
        messageText.setTextColor(textColor);
    }

}