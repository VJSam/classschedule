package com.vjs.classesschedule.extensions;

/**
 * Created by Victor on 17.10.2017.
 */

public class BaseItemClass {
    public Integer ID;
    public String Name;

    public BaseItemClass(int pID, String pName) {
        ID = pID;
        Name = pName;
    }
}
