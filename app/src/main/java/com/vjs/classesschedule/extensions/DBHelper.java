package com.vjs.classesschedule.extensions;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import com.vjs.classesschedule.lessons.LessonItemClass;

import java.io.File;
import java.sql.Time;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    final String LOG_TAG = "DBHelper";
    public static DBHelper dbHelper;
    private SQLiteDatabase dbSQL;
    private static String[] tables = new String[] {"rooms", "lessons", "student_groups", "schedule"};
    private String lastError = "";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version,
                    DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DBHelper(Context context) {
        super(context, getNameDB(), null, 1);
        dbHelper = this;
        dbSQL = dbHelper.getWritableDatabase();
        checkPreparedDB();
    }

    public static synchronized String getNameDB() {
        String storage_folder = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/ClassSchedule/database";
        File f = new File(storage_folder);
        if (!f.exists()) {
            f.mkdirs();
        }
        return storage_folder + "/cScheduleDB";
    }

    public static synchronized DBHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new DBHelper(context);
        }
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String getLastError() {
        return lastError;
    }

    public boolean execSQL(String text) {
        boolean res = false;
        lastError = "";

        try {
            dbSQL.execSQL(text);
            res = true;
        } catch (Exception ex) {
            lastError = ex.getMessage();
        }
        return res;
    }

    private void checkPreparedDB() {
        for (int i = 0; i < tables.length; i++) {
            String t = tables[i];
            Cursor cur = dbSQL.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + t + "'", null);
            if (cur.getCount() == 0) {
                switch (i) {
                    case 0 : createRoomsTable(); break;
                    case 1 : createLessonsTable(); break;
                    case 2 : createStudentGroupsTable(); break;
                    case 3 : createScheduleTable(); break;
                }
            }
            cur.close();
        }
    }

    private void createRoomsTable() {
        execSQL("CREATE TABLE `rooms` (\n" +
                "\t`room_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t`name`\tTEXT NOT NULL,\n" +
                "\t`deleted`\tINTEGER NOT NULL DEFAULT 0)");
    }

    private void createLessonsTable() {
        execSQL("CREATE TABLE `lessons` (\n" +
                "\t`lesson_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`name`\tTEXT NOT NULL,\n" +
                "\t`deleted`\tINTEGER NOT NULL DEFAULT 0)");
    }

    private void createStudentGroupsTable() {
        execSQL("CREATE TABLE `student_groups` (\n" +
                "\t`student_group_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`name`\tTEXT NOT NULL,\n" +
                "\t`deleted`\tINTEGER NOT NULL DEFAULT 0)");
    }

    private void createScheduleTable() {
        execSQL("CREATE TABLE `schedule` (\n" +
                "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t`start_time`\tNUMERIC NOT NULL,\n" +
                "\t`lesson_id`\tINTEGER NOT NULL,\n" +
                "\t`group_id`\tINTEGER NOT NULL,\n" +
                "\t`room_id`\tINTEGER NOT NULL,\n" +
                "\t`day_id`\tINTEGER NOT NULL DEFAULT 1,\n" +
                "\t`denom`\tINTEGER NOT NULL DEFAULT 0,\n" +
                "\t`deleted`\tINTEGER NOT NULL DEFAULT 0\n" +
                ");");
    }

    public boolean addLesson(String text) {
        boolean res = execSQL("INSERT INTO `lessons` (name)\n" +
                "VALUES ('" + text + "')");

        return res;
    }

    public boolean editLesson(int ID, String text) {
        boolean res = execSQL("UPDATE `lessons`\n" +
                "SET name = '" + text + "' WHERE lesson_id = " + String.valueOf(ID));

        return res;
    }

    public boolean deleteLesson(int ID) {
        boolean res = execSQL("UPDATE `lessons`\n" +
                "SET deleted = 1 WHERE lesson_id = " + String.valueOf(ID));

        return res;
    }

    public boolean removeLesson(int ID) {
        boolean res = execSQL("delete from `lessons`\n" +
                "WHERE lesson_id = " + String.valueOf(ID));

        return res;
    }

    public ArrayList<SimpleListItem> getLessonList() {
        ArrayList<SimpleListItem> res = null;

        String deleted = null;
//        deleted = "deleted = 0";

        Cursor cur = dbSQL.query("lessons",null, deleted,
                                 null, null, null, "`name` ASC");

        if (cur != null) {
            res = new ArrayList<SimpleListItem>();
            if (cur.moveToFirst()) {
                do {
                    res.add(new SimpleListItem(cur.getInt(cur.getColumnIndex("lesson_id")),
                                               cur.getString(cur.getColumnIndex("name")),
                                               cur.getInt(cur.getColumnIndex("deleted"))));


                } while (cur.moveToNext());
            }
            cur.close();
        }

        return res;
    }

    public boolean addGroup(String text) {
        boolean res = execSQL("INSERT INTO `student_groups` (name)\n" +
                "VALUES ('" + text + "')");

        return res;
    }

    public boolean editGroup(int ID, String text) {
        boolean res = execSQL("UPDATE `student_groups`\n" +
                "SET name = '" + text + "' WHERE student_group_id = " + String.valueOf(ID));

        return res;
    }

    public boolean deleteGroup(int ID) {
        boolean res = execSQL("UPDATE `student_groups`\n" +
                "SET deleted = 1 WHERE student_group_id = " + String.valueOf(ID));

        return res;
    }

    public boolean removeGroup(int ID) {
        boolean res = execSQL("delete from `student_groups`\n" +
                "WHERE student_group_id = " + String.valueOf(ID));

        return res;
    }

    public ArrayList<SimpleListItem> getGroupList() {
        ArrayList<SimpleListItem> res = null;

        String deleted = null;
//        deleted = "deleted = 0";

        Cursor cur = dbSQL.query("student_groups",null, deleted,
                null, null, null, "`name` ASC");

        if (cur != null) {
            res = new ArrayList<SimpleListItem>();
            if (cur.moveToFirst()) {
                do {
                    res.add(new SimpleListItem(cur.getInt(cur.getColumnIndex("student_group_id")),
                            cur.getString(cur.getColumnIndex("name")),
                            cur.getInt(cur.getColumnIndex("deleted"))));


                } while (cur.moveToNext());
            }
            cur.close();
        }

        return res;
    }

    public boolean addRoom(String text) {
        boolean res = execSQL("INSERT INTO `rooms` (name)\n" +
                "VALUES ('" + text + "')");

        return res;
    }

    public boolean editRoom(int ID, String text) {
        boolean res = execSQL("UPDATE `rooms`\n" +
                "SET name = '" + text + "' WHERE room_id = " + String.valueOf(ID));

        return res;
    }

    public boolean deleteRoom(int ID) {
        boolean res = execSQL("UPDATE `rooms`\n" +
                "SET deleted = 1 WHERE room_id = " + String.valueOf(ID));

        return res;
    }

    public boolean removeRooms(int ID) {
        boolean res = execSQL("delete from `rooms`\n" +
                "WHERE room_id = " + String.valueOf(ID));

        return res;
    }

    public ArrayList<SimpleListItem> getRoomsList() {
        ArrayList<SimpleListItem> res = null;

        String deleted = null;
//        deleted = "deleted = 0";

        Cursor cur = dbSQL.query("rooms",null, deleted,
                null, null, null, "`name` ASC");

        if (cur != null) {
            res = new ArrayList<SimpleListItem>();
            if (cur.moveToFirst()) {
                do {
                    res.add(new SimpleListItem(cur.getInt(cur.getColumnIndex("room_id")),
                            cur.getString(cur.getColumnIndex("name")),
                            cur.getInt(cur.getColumnIndex("deleted"))));


                } while (cur.moveToNext());
            }
            cur.close();
        }

        return res;
    }

    public boolean addSchedule(int lessonID, int roomID, int groupID, int dayID, int time, boolean isDenom) {
        int denom = 0;
        if (isDenom)
            denom = 1;
        
        boolean res = execSQL("INSERT INTO `schedule` (lesson_id, room_id, group_id, day_id, start_time, denom)\n" +
                                "VALUES (" + String.valueOf(lessonID) + ", " +
                                String.valueOf(roomID) + ", " +
                                String.valueOf(groupID) + ", " +
                                String.valueOf(dayID) + ", " +
                                String.valueOf(time) + ", " +
                                String.valueOf(denom) + ")" );

        return res;
    }

    public ArrayList<DayItemClass> getScheduleList(boolean isDenom) {
        ArrayList<DayItemClass> res = null;

        String denom = "0";
        if (isDenom)
            denom = "1";

        Cursor cur = dbSQL.rawQuery("select s.id, s.denom, s.day_id, l.name lesson_name, r.name room_name, g.name group_name, s.start_time\n" +
                "from schedule s\n" +
                "join lessons l on l.lesson_id = s.lesson_id\n" +
                "join rooms r on r.room_id = s.room_id\n" +
                "join student_groups g on g.student_group_id = s.group_id\n" +
                "where s.deleted = 0 and s.denom = " + denom +
                "\norder by s.denom, s.day_id, s.start_time", null);

        if (cur != null) {
            res = new ArrayList<DayItemClass>();
            DayItemClass d = new DayItemClass(0);
            if (cur.moveToFirst()) {
                do {
                    int dayID = cur.getInt(cur.getColumnIndex("day_id"));
                    if (d.getDayID() != dayID) {
                        if (d.getDayID() == 0) {
                            d.setDayID(dayID);
                        } else {
                            res.add(d);
                            d = new DayItemClass(dayID);
                        }
                    }
                    int time = cur.getInt(cur.getColumnIndex("start_time"));
                    int m = time % 100;
                    int h = (time - m)/100;
                    String stime = String.valueOf(h) + ":" + String.valueOf(m);
                    if (stime.length() == 3)
                        stime = "0" + stime;

                    Time t = Time.valueOf(stime + ":00");
                    d.Lessons.add(cur.getInt(cur.getColumnIndex("id")), t,
                            cur.getString(cur.getColumnIndex("lesson_name")),
                            cur.getString(cur.getColumnIndex("room_name")),
                            cur.getString(cur.getColumnIndex("group_name")));


                } while (cur.moveToNext());
                res.add(d);
            }
            cur.close();
        }

        return res;
    }

    public LessonItemClass getLessonByID(int lessonID) {
        LessonItemClass res = new LessonItemClass();

        Cursor cur = dbSQL.rawQuery("select s.*, l.name lesson_name, r.name room_name, g.name group_name\n" +
                "from schedule s\n" +
                "join lessons l on l.lesson_id = s.lesson_id\n" +
                "join rooms r on r.room_id = s.room_id\n" +
                "join student_groups g on g.student_group_id = s.group_id\n" +
                "where s.deleted = 0 and s.id = " + String.valueOf(lessonID), null);

        if (cur != null) {

            if (cur.moveToFirst()) {
                do {
                    int time = cur.getInt(cur.getColumnIndex("start_time"));
                    int m = time % 100;
                    int h = (time - m)/100;
                    String stime = String.valueOf(h) + ":" + String.valueOf(m);
                    if (stime.length() == 3)
                        stime = "0" + stime;

                    Time t = Time.valueOf(stime + ":00");
                    res.ID = cur.getInt(cur.getColumnIndex("id"));
                    res.BeginTime = t;
                    res.Name = cur.getString(cur.getColumnIndex("lesson_name"));
                    res.Place = cur.getString(cur.getColumnIndex("room_name"));
                    res.GroupName = cur.getString(cur.getColumnIndex("group_name"));
                    res.LessonIDs.RoomID = cur.getInt(cur.getColumnIndex("room_id"));
                    res.LessonIDs.GroupID = cur.getInt(cur.getColumnIndex("group_id"));
                    res.LessonIDs.PairID = cur.getInt(cur.getColumnIndex("lesson_id"));
                    res.LessonIDs.DayID = cur.getInt(cur.getColumnIndex("day_id"));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return res;
    }

    public boolean removeSchedule(int pID) {
        boolean res = execSQL("delete from `schedule`\n" +
            "WHERE id = " + String.valueOf(pID));

        return res;
    }

    public boolean deleteSchedule(int pID) {
        boolean res = execSQL("UPDATE `schedule`\n" +
                "SET deleted = 1 WHERE id = " + String.valueOf(pID));

        return res;
    }

    public boolean editSchedule(int ID, int dayID, int lessonID, int roomID, int groupID, int time) {
        boolean res = execSQL("UPDATE `schedule`\n" +
                "SET day_id = " + String.valueOf(dayID) + ", \n" +
                        "lesson_id = " + String.valueOf(lessonID) + ", \n" +
                "room_id = " + String.valueOf(roomID) + ", \n" +
                "group_id = " + String.valueOf(groupID) + ", \n" +
                "start_time = " + String.valueOf(time) + " \n" +
                "WHERE id = " + String.valueOf(ID));

        return res;
    }
}