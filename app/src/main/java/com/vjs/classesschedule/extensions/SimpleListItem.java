package com.vjs.classesschedule.extensions;

public class SimpleListItem {
    public int ID = -1;
    public String Name = "";
    public int Deleted = 0;

    public SimpleListItem(int pID, String pName, int pDeleted) {
        ID = pID;
        Name = pName;
        Deleted = pDeleted;
    }

    @Override
    public String toString() {
        return Name;
    }
}
