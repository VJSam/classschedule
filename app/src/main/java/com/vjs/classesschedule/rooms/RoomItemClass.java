package com.vjs.classesschedule.rooms;

import com.vjs.classesschedule.extensions.BaseItemClass;

/**
 * Created by Victor on 17.10.2017.
 */

public class RoomItemClass extends BaseItemClass {

    public RoomItemClass(int pID, String pName) {
        super(pID, pName);
    }
}
