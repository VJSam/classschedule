package com.vjs.classesschedule.rooms;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vjs.classesschedule.R;
import com.vjs.classesschedule.modules.BaseListAdapter;

import static com.vjs.classesschedule.extensions.Extensions.PAGE_ROOMS;

public class RoomsListAdaper extends BaseListAdapter {

    public RoomsListAdaper(Context context, RoomsClass rooms, View view) {
        super(context, view);
        list = rooms;
    }

    @Override
    public RoomsClass getList() {
        return (RoomsClass)list;
    }
}