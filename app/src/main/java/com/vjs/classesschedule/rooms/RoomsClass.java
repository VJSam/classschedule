package com.vjs.classesschedule.rooms;

import com.vjs.classesschedule.extensions.BaseClass;

public class RoomsClass extends BaseClass {

    public RoomsClass() {
    }

    public void add(Integer pID, String pName) {
        RoomItemClass lessonItem = new RoomItemClass(pID, pName);
        add(lessonItem);
    }

    public int count() {
        return this.size();
    }

    public RoomItemClass get(int index) {
        return (RoomItemClass) super.get(index);
    }
}
